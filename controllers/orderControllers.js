const Order = require('./../models/Order')
const Product = require('./../models/Product')
const moment = require('moment-timezone');
// let dateManila = moment.tz(Date.now(), "Asia/Manila").format("MM-DD-YYYY hh:mm A");
// console.log(dateManila)


//create order
module.exports.createOrder = (userId) => {
	let newOrder = new Order({
		purchasedOn: moment.tz(Date.now(), "Asia/Manila").format("MM-DD-YYYY hh:mm A"),
		userId: userId
		
	})

	//Model.prototype.method
	return newOrder.save().then( (order, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
		
//add to order
module.exports.addToOrder = (userId,reqBody) => {
	let productId = reqBody.productId;
	let quantity = reqBody.quantity;
	// console.log("try")
	// console.log(productId)
	return Order.findOne({userId: userId, isCurrent: true}).then(result => {
		if (result) {
			// console.log("try2")
			// console.log(result);
			let duplicate = result.products.find(element => element.productId === productId);
			// console.log(duplicate);
			if(duplicate){

						// console.log("existing")
						let inc = duplicate.quantity + 1
						// console.log(inc)
						// console.log("after quantity")
						// console.log(result)
						
						return Order.findOneAndUpdate({userId: userId, isCurrent: true},
							{
								$inc: {'products.$[element].quantity': quantity}
							}, {
								arrayFilters: [{"element.productId": productId}],
								new : true
							} ).then( (order, error) => {
								if(error){
									return false
								} else {
									return order
								}
						})
						
					} else {
						// console.log("not existing")

						return Product.findById(reqBody.productId).then((product) => {
							// console.log("try3")
							// console.log(productId)
							result.products.push({productId:productId, name:product.name, description: product.description,imageUrl: product.imageUrl, price: product.price, quantity: quantity  });
							// console.log(result.products);

							return result.save().then( (order, error) => {
								if(error){
									return false
								} else {
									return order
								}
						})

						})
					}

		} else {
			return false
		}

	})
}

module.exports.incQuantity = (userId,reqBody) => {
	let productId = reqBody.productId;
		return Order.findOneAndUpdate({userId: userId, isCurrent: true},
			{
				$inc: {'products.$[element].quantity': 1}
			}, {
				arrayFilters: [{"element.productId": productId}],
				new : true
			} ).then( (order, error) => {
				if(error){
					return false
				} else {
					return order
				}
		})
		
	}
		


module.exports.decQuantity = (userId,reqBody) => {
	let productId = reqBody.productId;
		return Order.findOneAndUpdate({userId: userId, isCurrent: true},
			{
				$inc: {'products.$[element].quantity': -1}
			}, {
				arrayFilters: [{"element.productId": productId}],
				new : true
			} ).then( (order, error) => {
				if(error){
					return false
				} else {
					return order
				}
		})
		
	}


	module.exports.removeProduct = (userId,reqBody) => {
		let productId = reqBody.productId;
			return Order.findOneAndUpdate({userId: userId, isCurrent: true},
				{ $pull: { products: { productId: productId} } }, {
					multi : true
				} ).then( (order, error) => {
					if(error){
						return false
					} else {
						return true
					}
			})
			
		}



module.exports.checkOut = (userId,reqBody) => {
	let totalAmount = reqBody.totalAmount;
	let deliveryOption = reqBody.deliveryOption;
	let dateTime = reqBody.dateTime;
	let payment = reqBody.payment;
	let referenceName = reqBody.referenceName;
	let referenceNum = reqBody.referenceNum;
	let fullNameOrder = reqBody.fullNameOrder;
	let mobileNoOrder = reqBody.mobileNoOrder;
	let addressLineOrder = reqBody.addressLineOrder;
	let facebookName = reqBody.facebookName;
	return Order.findOneAndUpdate({userId: userId, isCurrent: true},
		{
			$set: {isCurrent: false, purchasedOn:moment.tz(Date.now(), "Asia/Manila").format("MM-DD-YYYY hh:mm A"), 
			totalAmount: totalAmount, 
			status: "pending",
			deliveryOption: deliveryOption,
			dateTime: dateTime,
			payment: payment,
			referenceName: referenceName,
			referenceNum: referenceNum,
			fullNameOrder: fullNameOrder,
			mobileNoOrder: mobileNoOrder,
			addressLineOrder: addressLineOrder,
			facebookName: facebookName


		}
		}, {
			new : true
		} ).then( (order, error) => {
			if(error){
				return false
			} else {
				return order
			}
	})


}

module.exports.completeOrder = (reqBody) => {
	// let orderId = reqBody.orderId;
	console.log(reqBody.orderId)
	return Order.findOneAndUpdate({_id: reqBody.orderId, isCurrent: false},
		{
			$set: {purchasedOn:moment.tz(Date.now(), "Asia/Manila").format("MM-DD-YYYY hh:mm A"),status: "completed"}
		}, {
			new : true
		} ).then( (order, error) => {
			if(error){
				return false
			} else {
				return order
			}
	})


}



module.exports.getAllOrders = () => {
	//Model.method
	return Order.find().then( result => {
		return result
	})
}

//get single order
module.exports.getSingleOrder = (userId) => {
	//console.log(params) //{ productId: '6131b2f45528f7bcdda85a56' }
	//Model.method
	return Order.findOne({userId: userId, isCurrent: true}).then( (order, error) => {
		if (error) {
			return false
		} else {
		return order 
		}
	})

}

module.exports.getAllPrevOrders = (userId) => {
	return Order.find({userId: userId, isCurrent: false}).then( (order, error) => {
		if (error) {
			return false
		} else {
		return order 
		}
	})

}

