const Product = require('./../models/Product')

module.exports.getAllActive = () => {
	//Model.method
	return Product.find({isActive: true}).then( result => {
		
		return result
	})
}

module.exports.getAllProducts = () => {
	//Model.method
	return Product.find().then( result => {
		return result
	})
}

module.exports.addProduct = (reqBody) => {
	// console.log(reqBody)

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks,
		imageUrl: reqBody.imageUrl
	})

	//Model.prototype.method
	return newProduct.save().then( (product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//get single product
module.exports.getSingleProduct = (reqBody) => {
	//console.log(params) //{ productId: '6131b2f45528f7bcdda85a56' }
	//Model.method
	return Product.findById(reqBody.id).then( (product, error) => {
		if (error) {
			return false
		} else {
		return product 
		}
	})

}


//updateProduct
module.exports.updateProduct = (reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}
	//Model.method
	return Product.findByIdAndUpdate(reqBody.id, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//archive product
module.exports.archiveProduct = (reqBody) => {

	let updatedActiveProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(reqBody.id, updatedActiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//unarchive product
module.exports.unarchiveProduct = (reqBody) => {

	let updatedActiveProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(reqBody.id, updatedActiveProduct, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//delete product
module.exports.deleteProduct = (reqBody) => {

	return Product.findByIdAndDelete(reqBody.id).then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//updateProduct
/*module.exports.updateStocks = (reqBody) => {
	let input = [
	    {
	        "productId": "61a0789c021a78f7099d3c20",
	        "quantity": 5

	    },
	    {
	        "productId": "619da2e4d1bbcd1cd8fd179a",
	        "quantity": 13
	    }
	]
	let updatedProduct = {
		$inc: { "stocks": - reqBody.quantity }
	}
	//Model.method
	return Product.findByIdAndUpdate(i.productId, { $inc: { "stocks": - i.quantity } }, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}*/

module.exports.updateStocks = (reqBody) => {
/*	let input = [
	    {
	        "productId": "61a0789c021a78f7099d3c20",
	        "quantity": 5

	    },
	    {
	        "productId": "619da2e4d1bbcd1cd8fd179a",
	        "quantity": 13
	    }
	]*/
	/*let updatedProduct = {
		$inc: { "stocks": - reqBody.quantity }
	}*/
	//Model.method
	let arrProd = reqBody.products;
	console.log(arrProd)
	return Product.bulkWrite(arrProd
        // Map your array to a query bulkWrite understands
        .map(point => {
            return {
                updateOne: {
                    filter: {
                        _id: point.productId
                    },
                    update:{ $inc: { "stocks": - point.quantity } },
                    upsert: true
                }
            };
        }))
	.then((result, error) => {
		if(error){
			return error
		} else {
			return true
		}
	})
}
/*
db
    .getCollection("products")
    .bulkWrite([
	    {
	        "productId": ObjectId("61a0789c021a78f7099d3c20"),
	        "quantity": 5

	    },
	    {
	        "productId": ObjectId("619da2e4d1bbcd1cd8fd179a"),
	        "quantity": 13
	    }
	]
        // Map your array to a query bulkWrite understands
        .map(point => {
            return {
                updateOne: {
                    filter: {
                        _id: point.productId
                    },
                    update:{ $inc: { "stocks": - point.quantity } },
                    upsert: true
                }
            };
        }));*/