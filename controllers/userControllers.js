
const User = require("./../models/User");
const Product = require('./../models/Product');
const Order = require('./../models/Order');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


module.exports.checkEmailExists = (reqBody) => {
	// console.log(reqBody)
	//model.method
	return User.find({$or: [
    {email: reqBody.email},
    {facebookName: reqBody.facebookName}
]})
	.then( (result) => {
		if(result.length != 0){ 
			return true //existing
		} else {
			return false
		}
	})
}

module.exports.register = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		facebookName: reqBody.facebookName,
		contactNo: reqBody.contactNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error;
		} else {
			return result;
		}
	});
};


module.exports.login = (reqBody) => { 
	//Model.method
	return User.findOne({$or: [
    {email: reqBody.username},
    {facebookName:{ $regex: new RegExp("^" + reqBody.username + '$', "i") }}
]}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) //boolean return

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	console.log(data)
	//Model.method
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}


//change role
module.exports.changeToAdmin = (reqBody) => {

	let updatedRole = {}

		if (reqBody.isAdmin === false) {
			updatedRole = {isAdmin: true} 
		} else {
			updatedRole = {isAdmin: false}
		}
	

	return User.findByIdAndUpdate(reqBody.id, updatedRole, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.getAllUsers = () => {
	//Model.method
	return User.find().then( result => {
		return result
	})
}

//add to addresses
module.exports.addNewAddress = (userEmail,reqBody) => {
	// let isDefault = reqBody.isDefault;
	let fullName = reqBody.fullName;
	let mobileNo = reqBody.mobileNo;
	let addressLine = reqBody.addressLine;
	let addressIdReq ={}
	// let email = reqBody.email;
	// console.log("try")
	// console.log(productId)
	return User.findOne({email: userEmail}).then(result => {
		if (result.addresses.length==0) {
			
				result.addresses.push({isDefault:true, fullName:fullName, mobileNo: mobileNo, addressLine: addressLine});
				// console.log(result.products);
				return result.save().then( (user, error) => {
					if(error){
						return false
					} else {
						return user
					}
			})

			
	

		} else {
				result.addresses.push({isDefault:false, fullName:fullName, mobileNo: mobileNo, addressLine: addressLine});
				// console.log(result.products);
				return result.save().then( (user, error) => {
					if(error){
						return false
					} else {
						addressIdReq = {"addressId" :user.addresses[user.addresses.length -1]._id} 
						return addressIdReq
					}
			})

		}

	})
}

module.exports.getAllAddresses = (userEmail) => {
	//Model.method
	return User.findOne({email: userEmail}).then( result => {
		return result.addresses
	})
}



//set all setAsDefault to false
module.exports.clearDefault = (userEmail) => {

	// let	updatedRole = {isDefault: false}
	console.log(userEmail)
	

	return User.findOneAndUpdate({email: userEmail},
			{
				$set: {'addresses.$[element].isDefault': false}
			}, {
				arrayFilters: [{"element.isDefault": true}],
				new : true
			} )
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//set this address to setAsDefault to true
module.exports.setAsDefault = (userEmail,reqBody) => {

	let addressId = reqBody.addressId;
	// let mobileNo = reqBody.mobileNo;
	// let addressLine = reqBody.addressLine;
	

	return User.findOneAndUpdate({email: userEmail},
			{
				$set: {'addresses.$[element].isDefault': true}
			}, {
				arrayFilters: [{"element._id": addressId/*,"element.fullName": fullName,"element.fullName": fullName*/}]
				// new : true
			} )
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

	module.exports.removeAddress = (userId,reqBody) => {
		let addressId = reqBody.addressId;
			return User.findOneAndUpdate({_id: userId},
				{ $pull: { addresses: { _id: addressId} } }, {
					multi : true
				} ).then( (user, error) => {
					if(error){
						return false
					} else {
						return true
					}
			})
			
		}

		//updateAddress
module.exports.updateAddress = (userId,reqBody) => {

	let fullName = reqBody.fullName;
	let mobileNo = reqBody.mobileNo;
	let addressLine = reqBody.addressLine;
	let addressId = reqBody.addressId;
	//Model.method
	return User.findOneAndUpdate({_id: userId},
			{
				$set: {'addresses.$[element].fullName': fullName,'addresses.$[element].mobileNo': mobileNo,'addresses.$[element].addressLine': addressLine}
			}, {
				arrayFilters: [{"element._id": addressId}]
				// new : true
			} )
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.updateDetails = (userId,reqBody) => {
	let email = reqBody.email;
	let facebookName = reqBody.facebookName;
	let contactNo = reqBody.contactNo;
	return User.findOneAndUpdate({_id: userId},
		{
			$set: { 
			email: email, 
			facebookName: facebookName,
			contactNo: contactNo
		}
		}, {
			new : true
		} ).then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
	})
}


// password: bcrypt.hashSync(reqBody.password, 10)

module.exports.changePassword = (userId,reqBody) => {

	return User.findOneAndUpdate({_id: userId},
		{
			$set: { 
			password: bcrypt.hashSync(reqBody.password, 10)
		}
		}, {
			new : true
		} ).then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
	})
}
