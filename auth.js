let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";
// const User = require("./../models/User");

//create token
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		facebookName: user.facebookName,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	// console.log(token); //bearer jkhsdjgslkdfh'psuigjkad

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})

	}
}

//decode token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}

		})
	}
}
/*
module.exports.getSingleOrder = (req, res, next) => {
	//console.log(params) //{ productId: '6131b2f45528f7bcdda85a56' }
	//Model.method
	let token = req.headers.authorization

	// console.log(token); //bearer jkhsdjgslkdfh'psuigjkad

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return User.findOne({userId: userId, isCurrent: true}).then( (order, error) => {
		if (error) {
			return false
		} else {
			next(); 
		}
	})
		

	}

	

}

router.get("/orders", auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization);

if(userData.isAdmin === false) {
res.send(false);
} else {
userController.getAllOrders().then(resultFromController => res.send(resultFromController));
}
})
module.exports.getAllOrders = () => {
return User.find({isAdmin: false}).then(users => {
let allOrders = [];
users.forEach(user => {
allOrders.push({
email: user.email,
userId: user._id,
orders: user.orders
});
})
return allOrders;
})
}
ung route and controller*/