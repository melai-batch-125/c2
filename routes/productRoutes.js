const express = require('express');
const router = express.Router();
let auth = require('./../auth');
//controllers
const productController = require('./../controllers/productControllers');

//retrieve active products
router.get('/active', (req, res) => {

	productController.getAllActive().then( result => res.send(result));
})

//retrieve all products
router.get("/all", auth.verify, (req, res) => {
	
		productController.getAllProducts().then( result => res.send(result))
	
})


//add product
router.post('/addProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		productController.addProduct(req.body).then( result => res.send(result))
	}
})


//make a route to get single product
router.post('/single', auth.verify, (req, res) => {

	productController.getSingleProduct(req.body).then( result => res.send(result))
})

//make route to update the product
router.put('/updateProduct', auth.verify, (req, res) => {
	//console.log(req.body)
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		productController.updateProduct(req.body).then( result => res.send(result))
	}
})


//make a route to archive the product
	//meaning, update the status of the product from true to false

router.put('/archivedProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		productController.archiveProduct(req.body).then( result => res.send(result))
	}
})

//make a route to unarchive the product
	//meaning, update the status of the product from false to true

router.put('/unarchiveProduct', auth.verify, (req, res) => {
	//console.log("try")
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		productController.unarchiveProduct(req.body).then( result => res.send(result)) 
	}
})


//make a route to delete a product
	//meaning, the product object will be deleted from the database
router.delete('/deleteProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		productController.deleteProduct(req.body).then( result => res.send(result))
	}
})
/*

let input = [
    {
        "productId": "61a0789c021a78f7099d3c20",
        "quantity": 5

    },
    {
        "productId": "619da2e4d1bbcd1cd8fd179a",
        "quantity": 13
    }
]
*/
router.put('/updateStocks', auth.verify, (req, res) => {
	//console.log(req.body)
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		productController.updateStocks(req.body).then( result => res.send(result))
	}
})

module.exports = router;
