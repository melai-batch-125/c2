const express = require('express');
const router = express.Router();
let auth = require('./../auth');
//controllers
const orderController = require('./../controllers/orderControllers');
const productController = require('./../controllers/productControllers');

//add to order
router.post('/addToOrder', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		orderController.addToOrder(userData.id,req.body).then( result => res.send(result))
	}
})

//increase quantity
router.post('/incQuantity', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		orderController.incQuantity(userData.id,req.body).then( result => res.send(result))
	}
})

//decrease quantity
router.post('/decQuantity', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		orderController.decQuantity(userData.id,req.body).then( result => res.send(result))
	}
})


//remove product
router.post('/removeProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		orderController.removeProduct(userData.id,req.body).then( result => res.send(result))
	}
})


//create new order
router.post('/createOrder', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		orderController.createOrder(userData.id).then( result => res.send(result))
	}
})



//Check out order
router.post('/checkOut', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		orderController.checkOut(userData.id,req.body)
		.then( resultOrder => orderController.createOrder(userData.id)
			.then( result => productController.updateStocks(resultOrder).then( result => res.send(result))))
			
			// .then( result => res.send(resultOrder)))
	}
})


router.post('/completeOrder', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		orderController.completeOrder(req.body).then( result => res.send(result))
	}
})




//retrieve all orders
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		orderController.getAllOrders().then( result => res.send(result))
	}
})


//make a route to get single order
router.post('/single', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
  console.log(token)

	orderController.getSingleOrder(token.id).then( result => res.send(result))
})


router.post('/allPrevOrder', auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	console.log(token.id)
	orderController.getAllPrevOrders(token.id).then( result => res.send(result))
})










module.exports = router;
