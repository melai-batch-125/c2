
const express = require("express");
const router = express.Router();
//contollers
const userController = require("./../controllers/userControllers");
const orderController = require('./../controllers/orderControllers');
//auth
const auth = require('./../auth');

//check if email exists
router.post("/checkEmail", (req, res) => {
	// console.log(req.body)
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

//user registration
router.post("/register", (req, res)=> {
	userController.register(req.body).then(result => 
		orderController.createOrder(result._id).then( result => res.send(result))
		//lack of auth.verify


		);
})

//login
router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})

//details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
		userController.getProfile(userData.id).then(result => res.send(result))
	
})

//change Role
router.put('/changeToAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		userController.changeToAdmin(req.body).then( result => res.send(result)) 
	}
})


//retrieve all users
router.get("/all", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === false) {
		res.send("You do not have access for this function.");
	} else {
		userController.getAllUsers().then( result => res.send(result))
	}
})


//retrieve all users
router.post("/addNewAddress", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.addNewAddress(userData.email,req.body).then( resultUser => 
			(req.body.isDefault == true) 
			?userController.clearDefault(userData.email).then( result => userController.setAsDefault(userData.email,resultUser).then( result => res.send(result)))
			:(resultUser == false) 
				? res.send(resultUser)
				: res.send(true)
		)
	}
})



//retrieve all addresses
router.get("/allAddresses", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.getAllAddresses(userData.email).then( result => res.send(result))
	}
})

//set as setAsDefault to true 
router.put('/setAsDefault', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.clearDefault(userData.email).then( result => 
			userController.setAsDefault(userData.email,req.body).then( result => res.send(result))
		) 
	}
})

//remove product
router.post('/removeAddress', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.removeAddress(userData.id,req.body).then( result => res.send(result))
	}
})

router.put('/updateAddress', auth.verify, (req, res) => {
	//console.log(req.body)
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.updateAddress(userData.id,req.body).then( result => res.send(result))
	}
})

router.put('/updateDetails', auth.verify, (req, res) => {
	//console.log(req.body)
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.updateDetails(userData.id,req.body).then( result => res.send(result))
	}

})

router.put('/changePassword', auth.verify, (req, res) => {
	//console.log(req.body)
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin === true) {
		res.send("You do not have access for this function.");
	} else {
		userController.changePassword(userData.id,req.body).then( result => res.send(result))
	}
})

module.exports = router;

