const mongoose = require('mongoose');
const moment = require('moment-timezone');
// const dateManila = moment.tz(Date.now(), "Asia/Manila").format("MM-DD-YYYY hh:mm A");


const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: String
	},
	isCurrent: {
		type: Boolean,
		default: true
	},
	status: {
		type: String,
		default: "current"
	},
	deliveryOption: {
		type: String
	},
	dateTime: {
		type: String
	},
	payment: {
		type: String
	},
	referenceName: {
		type: String
	},
	referenceNum: {
		type: String
	},		
	userId: {
		type: String,
		required: [true, "userId is required"]
	},
	fullNameOrder: {
		type: String
	},
	mobileNoOrder: {
		type: String
	},
	addressLineOrder: {
		type: String
	},
	facebookName: {
		type: String
	},
	products:[{ //array of objects

		productId: {
			type: String
		},
		name: {
			type: String
		},
		description: {
			type: String
		},
		imageUrl: {
			type: String
		},
		price: {
			type: Number
		},
		quantity: {
			type: Number
		}
	}]
		
	
});

module.exports = mongoose.model("Order", orderSchema);