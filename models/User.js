const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
	{
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		facebookName: {
			type: String,
			required: [true, "Facebook Name is required"]
		},
		contactNo: {
			type: String,
			required: [true, "Contact number is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		addresses:[{ //array of objects

			isDefault: {
				type: Boolean
			},
			fullName: {
				type: String
			},
			mobileNo: {
				type: String
			},
			addressLine: {
				type: String
			}
	}]
	}
);

module.exports = mongoose.model("User", userSchema);