const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Product is required"]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		stocks: {
			type: Number,
			min: 0,
			required: [true, "Stock is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		imageUrl: {
			type: String,
			required: [true, "Image Url is required"]
		}
	}
);

module.exports = mongoose.model("Product", productSchema);